#ESPtool-ck

We are including the ESP8266 build/flash helper tool by Christian Klippel
in this package. ESPtool-ck is released under the GNU Public Licence v2.0,
the terms and conditions of which you can read in the following URL:

https://www.gnu.org/licenses/old-licenses/gpl-2.0.html

The source code for ESPtool-ck is available in the following Git
repository:

https://github.com/igrr/esptool-ck
