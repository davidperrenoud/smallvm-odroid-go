#OpenOCD

We are including parts of OpenOCD in this package. OpenOCD is released
under the GNU Public Licence v2.0, the terms and conditions of which you
can read in the following URL:

https://www.gnu.org/licenses/old-licenses/gpl-2.0.html

The source code for OpenOCD is available in the following Git repository:

http://repo.or.cz/w/openocd.git
